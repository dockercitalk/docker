# docker

Docker image builds for the docker continuous integration talk.

To show django example:

* sudo docker pull dockerslides/django:master
* sudo docker run --rm --network host -it dockerslides/django:master /sbin/my_init

To show wagtail example:

* sudo docker pull dockerslides/wagtail:master
* sudo docker run --rm --network host -it dockerslides/wagtail:master /sbin/my_init


To show laravel example:

* sudo docker pull dockerslides/laravel:master
* sudo docker run --rm --network host -it dockerslides/laravel:master /sbin/my_init -- bash -l
