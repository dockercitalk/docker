#!/bin/sh
set -eu

image="$1"

# Pull the latest version of the image.
echo "Pulling latest dockerhub image for ${image}:$environment"
starting_digest=`docker pull "${DOCKERHUB_ROOT}/${image}:$environment" | sed -n -e 's/.*sha256:\([a-f0-9]*\).*/\1/p'`

# Build the image.
docker build --cache-from "${DOCKERHUB_ROOT}/${image}:$environment" \
       --build-arg ENVIRONMENT=$environment \
       --build-arg DOCKERHUB_ROOT=$DOCKERHUB_ROOT \
       -t "${DOCKERHUB_ROOT}/${image}:$environment" \
       -f "${image}/${image}.docker" "${image}/"

# Push to dockerhub and get the new digest.
ending_digest=`docker push "${DOCKERHUB_ROOT}/${image}:$environment" | sed -n -e 's/.*sha256:\([a-f0-9]*\).*/\1/p'`

# We can use this to see if the image changed.
# This way we can trigger a deployment only if the image was changed.
echo "Starting digest = $starting_digest"
echo "Ending digest = $ending_digest"
